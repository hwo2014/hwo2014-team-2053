import sys
import HWOBot
import signal


class ConstantBot(HWOBot.HWOBot):
    def __init__(self, host, port, name, key, throttle_value):
        super(ConstantBot, self).__init__(host, port, name, key)
        self.constant_throttle = float(throttle_value)

    def calculate_throttle(self):
        return self.constant_throttle


def signal_handler(signal, frame):
        print "Saving logs before abort"
        bot.save_data_logs()
        print "Done"
        sys.exit(signal)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    if len(sys.argv) != 6:
        print("Usage: ./run host port botname botkey throttle")
    else:
        host, port, name, key, throttle = sys.argv[1:6]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        bot = ConstantBot(host, port, name, key, throttle)
        bot.run()
