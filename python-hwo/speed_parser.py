import hwodata
import sys

if __name__ == "__main__":
    """ Used to parse a log file containing a string of messages and calculate
       the average speed at certain points
    """
    handler = hwodata.HWODataHandler([])
    handler.import_lines(sys.argv[1])
    handler.homogenize_data()
    handler.cache_car_data()

    if len(sys.argv) >= 4:
        from_tick = int(sys.argv[2])
        to_tick = int(sys.argv[3])
    else:
        from_tick = 80
        to_tick = 100

    print "-- Pieces -- "
    i = 0
    for piece in handler.pieces:
        print i, piece
        i += 1

    print "-- Lanes --"
    for l in handler.lanes:
        print l

    print "-- Total length --"
    for l in handler.piece_lane_length_ranges:
        print l

    print "-- Speed and total positions --"
    for i in range(from_tick, to_tick):
        print i, handler.speed_at_tick(i, 10), handler.acceleration_at_tick(i, 10), handler.car_track_distance_at_tick(
            i)

    print "-- Speed before crashes --"
    for crash in handler.crashes:
        tick = crash["gameTick"]
        last_position = next(x for x in handler.car_positions if x["gameTick"] == tick - 1)
        car_position = hwodata.HWODataHandler.car_from_car_positions(handler.car_data["name"],
                                                                     last_position["data"])
        angle_acceleration = handler.angle_acceleration_at_tick(tick - 1, 5)
        piece_index = car_position["piecePosition"]["pieceIndex"]
        piece_angle = handler.pieces[piece_index]["angle"]

        print "Crash at tick {0}. Speed {1}".format(tick, handler.speed_at_tick(tick, 10))

        print "Piece index: {0}({2}), car angle: {1}, angle acc: {3}".format(piece_index, car_position["angle"],
                                                                             piece_angle, angle_acceleration)
        print "Angle history", handler.angle_history_at_tick(tick - 1, 5)


    print "-- Speed stats --"
    game_ticks = max(x["gameTick"] for x in handler.car_positions if "gameTick" in x)
    print "Ticks", game_ticks
    speeds = [handler.speed_at_tick(i, 10) for i in range(0, game_ticks + 1)]
    # Calculate a normal speed average sinc ean EMA is not relevant on the large data volume
    avg_speed = sum(speeds) / len(speeds)
    print "Max speed", max(speeds)
    print "Avg speed", avg_speed

    print "-- Track stats --"
    length = sum(x["length"] for x in handler.pieces)
    min_length = sum(min(x["lane_lengths"]) for x in handler.pieces)
    max_length = sum(max(x["lane_lengths"]) for x in handler.pieces)
    print "Avg length", length
    print "Avg ticks", length / avg_speed
    print "Min length", min_length
    print "Min ticks", min_length / avg_speed