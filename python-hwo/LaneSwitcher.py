import sys
import signal
from HWOBot import HWOBot
from math import fsum


class LaneSwitcher(HWOBot):
    def __init__(self, host, port, name, key, throttle):
        super(LaneSwitcher, self).__init__(host, port, name, key)
        self.switch_on_piece = -1
        self.next_lane = None
        self.constant_throttle = float(throttle)

    def process_car_positions(self, data):
        super(LaneSwitcher, self).process_car_positions(data)
        # If we've passed the last piece where we were supposed to switch,
        # clear it (we can possibly want to switch on it again)
        if self.piece_index > self.switch_on_piece:
            self.switch_on_piece = -1
        # Very basic behavior - if we just changed track pieces and we have
        # upcoming angles, where the other lane would be shorter, switch.
        if self.switch_on_piece == self.piece_index + 1 and not self.next_lane is None:
            print "Switching lanes", self.piece_index, self.switch_on_piece, self.next_lane
            self.send_switch(self.next_lane)
            self.next_lane = None
        elif self.last_piece_index != self.piece_index \
                and self.piece_index >= 0:
            try:
                # Find the next upcoming switch
                upcoming_switch = next(x for x in self.hwo_data.switches
                                       if x["index"] > self.piece_index)

                # If we have already decided to switch on them, bail out
                if self.switch_on_piece == upcoming_switch["index"]:
                    return

                # We will then get a stretch of track to evaluate the length.
                # If there's an upcoming switch, we'll measure up to it, if
                # not we just get the end of the track.
                try:
                    end_piece = next(x for x in self.hwo_data.switches if x["index"] > upcoming_switch["index"])
                except StopIteration:
                    end_piece = self.hwo_data.pieces[-1]
                next_stretch = self.hwo_data.pieces[upcoming_switch["index"]:end_piece["index"]]

                # Accumulate the lane lengths and evaluate which one we want
                piece_lengths = [x["lane_lengths"] for x in next_stretch]
                lane_lengths = [fsum(x) for x in zip(*piece_lengths)]
                shorter_length = min(lane_lengths)
                shorter_lane = lane_lengths.index(shorter_length)
                our_length = lane_lengths[self.lane_index]

                # """
                print "Found upcoming switch on piece {0} while on {1}".format(upcoming_switch["index"],
                                                                               self.piece_index)
                print " -- End piece", end_piece["index"], lane_lengths
                print " -- Current lane: {0} Shorter lane: {1} Length {2}".format(self.lane_index,
                                                                                  shorter_lane,
                                                                                  shorter_length)
                # """
                if shorter_length < our_length:
                    lane_names = ["Left", "Right"]
                    self.next_lane = lane_names[shorter_lane]
                    self.switch_on_piece = upcoming_switch["index"]
                    print " -- Will switch lanes on piece ", self.switch_on_piece, self.next_lane

            except StopIteration:
                print "No upcoming turn or switch after piece index", self.piece_index

    def calculate_throttle(self):
        return self.constant_throttle


def signal_handler(signal, frame):
        print "Saving logs before abort"
        bot.save_data_logs()
        print "Done"
        sys.exit(signal)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    if len(sys.argv) != 6:
        print("Usage: ./run host port botname botkey throttle")
    else:
        host, port, name, key, throttle = sys.argv[1:6]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        bot = LaneSwitcher(host, port, name, key, throttle)
        bot.run()
