import math


def angle_for_segment(segment_length, radius):
    return segment_length * 180 / (math.pi * radius)


def arc_length(radius, angle, round_to_digits=2):
    return round(abs(angle * 3.15169 / 180 * radius), round_to_digits)


def ema(collection, alpha):
    """Calculates an exponential moving average for the values in a collection"""
    if alpha is None:
        alpha = 2.0 / (len(collection) + 1)
    one_minus = 1 - alpha
    accumulator = 0
    denominator = 0
    max_length = len(collection) - 1
    for i in range(max_length, -1, -1):
        multiplier = (one_minus ** (max_length - i))
        accumulator += collection[i] * multiplier
        denominator += multiplier
    if denominator != 0:
        return accumulator / denominator
    else:
        return 0


def chord_length(angles, radius):
    radius * math.sqrt(2 - 2 * math.cos(math.radians(angles)))


def sign(number):
    return math.copysign(1, number)
