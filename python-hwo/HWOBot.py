import json
import socket
import sys
import time
import hwodata
import signal
import math
from data import DataLogger


class HWOBot(object):
    def __init__(self, host, port, name, key):
        # Connection values
        self.host = host
        self.port = int(port)
        # Connect
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        # Store other members
        self._current_tick = None
        self.name = name
        self.key = key
        self.hwo_data = hwodata.HWODataHandler([])
        self.ratings = []
        self.message_log = None
        self.last_car_position = None
        self.last_throttle_sent = 0
        self.current_car_position = None
        self.is_crashed = False
        # Values for target speed and throttle
        self._desired_speed = 11
        self._car_angle = 0
        self._car_angle_acceleration = 0
        self._speed = 0
        self._acceleration = 0
        self._turbo_data = None
        self._is_turbo = False
        # Create an empty record for the data logs
        self.data_logs = {}

    @property
    def acceleration(self):
        return self._acceleration

    @property
    def car_angle(self):
        return self._car_angle

    @property
    def car_angle_acceleration(self):
        return self._car_angle_acceleration

    @property
    def piece(self):
        return self.hwo_data.pieces[self.piece_index]

    @property
    def piece_index(self):
        return hwodata.HWODataHandler.piece_index_from_car_position(self.current_car_position)

    @property
    def speed(self):
        return self._speed

    @property
    def tick(self):
        return self._current_tick

    @property
    def lane_index(self):
        return self.current_car_position["piecePosition"]["lane"]["startLaneIndex"] \
            if not self.current_car_position is None else -1

    @property
    def desired_speed(self):
        return self._desired_speed

    @desired_speed.setter
    def desired_speed(self, value):
        self._desired_speed = value

    @property
    def full_name(self):
        return self.__class__.__name__ + "." + self.name

    @property
    def is_turbo(self):
        return self._is_turbo

    @property
    def last_piece_index(self):
        return hwodata.HWODataHandler.piece_index_from_car_position(self.last_car_position)


    @property
    def turbo_data(self):
        return self._turbo_data

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data, "tick": self.tick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def send_throttle(self, throttle):
        self.last_throttle_sent = max(min(throttle, 1), 0)
        self.msg("throttle", self.last_throttle_sent)

    def send_switch(self, lane):
        self.msg("switchLane", lane)

    def send_turbo(self):
        self.msg("turbo", "BROOOOOOOOOM!")
        self._turbo_data = None

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        print("Initialized")
        # Cache the track pieces
        self.hwo_data.homogenize_data()
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.hwo_data.cache_car_data()
        print "Racing with car ", self.hwo_data.car_data["name"]
        print "Game ID: ", self.hwo_data.game_id
        self.data_logs["speed"] = DataLogger("../data", self.full_name,
                                             "speed_samples",
                                             self.hwo_data.game_id,
                                             "csv")
        self.ping()

    def on_car_positions(self, data):
        self.process_car_positions(data)
        if not self.is_crashed:
            self.send_throttle(self.calculate_throttle())
        else:
            self.ping()

        # Log the data
        # Yes, we're recalculating items that other methods might be
        # calculating as well, but this avoids coupling
        if "speed" in self.data_logs:
            log_record = {
                "tick": self.tick,
                "throttle": self.last_throttle_sent,
                "piece": self.piece_index,
                "piece_distance": self.current_car_position["piecePosition"]["inPieceDistance"],
                "speed": self.speed,
                "desired_speed": self.desired_speed,
                "angle": self.car_angle,
                "angle_in_radians": math.radians(self.car_angle),
                "angle_acceleration": self.car_angle_acceleration,
                "acceleration": self.acceleration
            }
            self.data_logs["speed"].add_item(log_record)

    def process_car_positions(self, data):
        """Called to process the car position information for a carPositions
           event. We expect it will be overridden by subclasses so that they
           can make their own decisions.
        """
        self.last_car_position = self.current_car_position
        self.current_car_position = hwodata.HWODataHandler.car_from_car_positions(self.name, data)
        if not self.current_car_position is None and not self.tick is None:
            self._car_angle = self.hwo_data.average_angles_at_tick(self.tick, 5)
            self._car_angle_acceleration = self.hwo_data.angle_acceleration_at_tick(self.tick, 5)
            self._speed = self.hwo_data.speed_at_tick(self.tick, 10)
            self._acceleration = self.hwo_data.acceleration_at_tick(self.tick, 5)

    def calculate_throttle(self):
        """Base method for calculating the throttle. We'll just return the
           current one.  Subclasses decide what to do.
        """
        return 0.5

    def on_crash(self, data):
        if data["name"] == self.name:
            self.is_crashed = True
            self._is_turbo = False
            print("CRAAAAAASH! Clearing throttle.")
            self.last_throttle_sent = 0

            print("Last few messages:")
            for x in self.hwo_data.messages[-3:]:
                print "\t", x
            print("... continuing")
        self.ping()

    def on_spawn(self, data):
        print("Respawned")
        self.is_crashed = False
        self.ping()

    def on_lap_finished(self, data):
        print "!!Finished lap!!", data["lapTime"]
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_turbo_available(self, data):
        print "!!!Turbo available!!!", data
        self._turbo_data = data
        self.ping()

    def on_turbo_start(self, data):
        print "!!!TURBO!!!", data
        self._is_turbo = True

    def on_turbo_end(self, data):
        print "Daawww. No turbo"
        self._is_turbo = False

    def save_data_logs(self):
        for log_key in self.data_logs:
            print "Saving data log", log_key
            self.data_logs[log_key].save_log()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'lapFinished': self.on_lap_finished,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
        }
        self.message_log = open("../logs/MessageLog.{0}.{1}.{2}.txt".format(time.time(),
                                                                            self.__class__.__name__,
                                                                            self.name), "w")
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            self.message_log.write(line)
            msg = self.hwo_data.append_message_line(line)
            msg_type, data = msg['msgType'], msg['data']
            # Get the tick before doing any processing
            if "gameTick" in msg:
                self._current_tick = msg["gameTick"]
            # And handle
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
        self.message_log.close()
        # Save the data logs, if any.
        self.save_data_logs()
        print "Done with message loop"


def signal_handler(signal, frame):
    print "Saving logs before abort"
    bot.save_data_logs()
    print "Done"
    sys.exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    print "ON HWO MOTHERFUCKER"
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        bot = HWOBot(host, port, name, key)
        bot.run()

