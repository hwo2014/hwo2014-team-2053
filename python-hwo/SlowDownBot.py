import sys
from HWOBot import HWOBot


class SlowDownBot(HWOBot):
    def __init__(self, host, port, name, key):
        super(SlowDownBot, self).__init__(host, port, name, key)
        self.current_throttle = 1.0
        self.ratings = []

    def add_up_track_angles(self):
        ratings = []
        for i in range(len(self.hwo_data.pieces)):
            rating = 0
            next_pieces = self.hwo_data.pieces[i + 1:i + 3]
            for piece in next_pieces:
                rating = rating + abs(piece["angle"])
            rating = min(rating, 80)
            ratings.append(rating)
        # Second pass ... each rating gets averaged with the one for the previous piece
        for i in range(1, len(ratings)):
            ratings[i] = (ratings[i] + ratings[i - 1]) / 2
        # Third pass ... lower the danger rating if the next piece is a straight
        for i in range(0, len(ratings) - 1):
            if self.hwo_data.pieces[i + 1]["angle"] == 0:
                ratings[i] /= 4
        return ratings

    def on_game_init(self, data):
        super(SlowDownBot, self).on_game_init(data)
        self.ratings = self.add_up_track_angles()

    def calculate_throttle(self):
        rating = self.ratings[self.piece_index]
        speed_rating = 1 - (rating / 100)

        self.current_throttle = (speed_rating + self.current_throttle) / 2

        avg = abs(self.car_angle)
        if avg > 15:
            self.current_throttle /= 4
            print("Too wide angle! Angle: {0}. Throttling to {1}".format(avg, self.current_throttle))

        return self.current_throttle


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        bot = SlowDownBot(host, port, name, key)
        bot.run()
