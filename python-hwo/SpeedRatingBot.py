import sys
import signal
from HWOBot import HWOBot


class SpeedRatingBot(HWOBot):
    def __init__(self, host, port, name, key):
        super(SpeedRatingBot, self).__init__(host, port, name, key)
        self.current_throttle = 1.0
        self.ratings = []

    def on_game_init(self, data):
        super(SpeedRatingBot, self).on_game_init(data)

    def calculate_throttle(self):
        acceleration = self.hwo_data.acceleration_at_tick(self.tick, 5)

        look_ahead = 100
        ticks = look_ahead / max(self.speed, sys.float_info.epsilon)

        future_speed = self.speed + acceleration * ticks
        current_distance = self.hwo_data.car_track_distance_at_tick(self.tick)
        future_distance = current_distance + future_speed * ticks

        try:
            future_piece_length = next(x for x in self.hwo_data.piece_lane_length_ranges if
                                       x[self.lane_index]["start"] <= future_distance <=
                                       x[self.lane_index]["end"])
            future_piece = self.hwo_data.pieces[future_piece_length[0]["index"]]
        except StopIteration:
            future_piece = self.hwo_data.pieces[0]

        angle = abs(max(future_piece["angle"], self.piece["angle"]))

        if angle > 40:
            self.desired_speed = 6.5
        elif angle > 20:
            self.desired_speed = 8.5
        else:
            self.desired_speed = 10

        if self.speed > self.desired_speed:
            throttle = 0
        else:
            throttle = 1

        print "[{0}.{1}] Throttle: {6} Speed: {2} Target: {3} " \
              "aCar: {4} aPiece {5}".format(self.tick, self.piece_index,
                                            self.speed, self.desired_speed,
                                            self.car_angle, angle, throttle)
        print "[{0}.{1}] Acc: {2} CD: {3} FD: {4} FP: {5}".format(self.tick, self.piece_index,
                                                                  acceleration,
                                                                  current_distance,
                                                                  future_distance,
                                                                  future_piece["index"])

        return throttle


def signal_handler(signal, frame):
    print "Saving logs before abort"
    bot.save_data_logs()
    print "Done"
    sys.exit(signal)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        bot = SpeedRatingBot(host, port, name, key)
        bot.run()
