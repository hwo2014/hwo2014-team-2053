import math
import sys
import LaneSwitcher
import signal
from hwomath import sign


class SlowOnBends(LaneSwitcher.LaneSwitcher):
    """Builds on the LaneSwitcher and slows down on a bend, gunning it
       back up when we're exiting it.  Speed ratings for the various
       bends are currently hardcoded based on my tests so far.
    """

    def __init__(self, host, port, name, key):
        super(SlowOnBends, self).__init__(host, port, name, key, 1)
        self.switch_on_piece = -1
        self.next_lane = None
        self.acceleration_per_tick = 0.15
        self.deceleration_per_tick = 0.05
        self.decaying_throttle = 1
        self.max_target_speed = 10
        self.max_distance_to_bend = 85

    def calculate_throttle(self):
        if self.tick is None:
            return 0
        try:
            # Get the next immediate bend... which could be our current one
            next_bend = next(x for x in self.hwo_data.bends if x["index"] >= self.piece_index)
        except StopIteration:
            next_bend = None

        tick_throttle = 1
        target_speed = self.max_target_speed
        if not next_bend is None:
            # Get the current piece length and how far we've gone. We'll use them later
            piece_length = self.piece["lane_lengths"][self.lane_index]
            piece_distance = self.current_car_position["piecePosition"]["inPieceDistance"]
            # If we have an upcoming bend which is not our piece
            if next_bend != self.piece:
                # Get the distance until the next bend for our current lane
                pieces = self.hwo_data.pieces[self.piece_index + 1:next_bend["index"]]
                piece_lengths = [x["lane_lengths"][self.lane_index] for x in pieces]
                to_bend = math.fsum(piece_lengths)

                # Calculate how far ahead we're on our current piece
                to_bend += piece_length - piece_distance
            else:
                to_bend = 0

            to_bend = max(to_bend, 0)
            if to_bend < self.max_distance_to_bend:
                target_speed = 6.5 if abs(next_bend["angle"]) >= 25 else 8.5
                distance_percent = to_bend / self.max_distance_to_bend
                target_speed += (self.max_target_speed - target_speed) * math.log1p(distance_percent)
            else:
                target_speed = self.max_target_speed

            # Calculate the car angle to consider lowering the target speed
            car_abs_angle = abs(self.car_angle)

            # If the distance is 0, that means we are in an angle piece. Check
            # if the next one is a straight, and if so accelerate proportionally
            # to how var out of it we are.
            #
            # We also apply the extra boost if the next piece is a turn BUT it
            # it is going in the opposite direction, which would actually help
            # straighten out the car.
            if to_bend == 0:
                next_piece = self.hwo_data.pieces[self.piece["index"] + 1]
                if (next_piece["angle"] == 0 or sign(next_piece["angle"]) != sign(
                        self.piece["angle"])) and car_abs_angle < 40:
                    # Do not give an extra boost if we're going out of control
                    print "On angle, next piece is not, increasing target speed"
                    piece_proportion = piece_distance / piece_length
                    target_speed += (self.max_target_speed - target_speed) * math.log1p(piece_proportion) / math.log1p(
                        1)

            if self.speed >= target_speed:
                # Don't throttle if we're going over our target speed, and decay
                # the target throttle every tick we don't throttle
                tick_throttle = 0
                self.decaying_throttle -= self.deceleration_per_tick
            else:
                self.decaying_throttle = max(self.decaying_throttle, target_speed / 10)
                self.decaying_throttle += self.acceleration_per_tick
                tick_throttle = self.decaying_throttle

            """
                Evaluate if to cut back the throttle based on the estimated car
                angle for the next tick. We can assume that any car that is at
                more than 90 degrees with the track will crash.
            """
            future_angle = self.car_angle + self.car_angle_acceleration + self.piece["angle"]
            if tick_throttle > 0 and abs(future_angle) > 90:
                tick_throttle /= 2

            print "[{0}.{1}] Speed: {2} Target: {3} aCar: {4}. aAcc: {5} aNext: {6}".format(self.tick,
                                                                                            self.piece_index,
                                                                                            self.speed,
                                                                                            target_speed, car_abs_angle,
                                                                                            self.car_angle_acceleration,
                                                                                            future_angle)
            if tick_throttle > 0:
                print "[{0}.{1}] Throttle T:{2} aPiece? {3} Dist. {4}".format(self.tick,
                                                                              self.piece_index,
                                                                              tick_throttle,
                                                                              self.piece["angle"],
                                                                              to_bend)
        else:
            to_bend = 300

        if not self.turbo_data is None and self.speed * self.turbo_data["turboDurationTicks"] < to_bend:
            print "[{0}.{1}] FUCK EVERYTHING, SENDING TURBO".format(self.tick, self.piece_index)
            self.send_turbo()

        self.decaying_throttle = min(max(self.decaying_throttle, 0), 1)
        self.desired_speed = target_speed

        return tick_throttle


def signal_handler(signal, frame):
    print "Saving logs before abort"
    bot.save_data_logs()
    print "Done"
    sys.exit(signal)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        bot = SlowOnBends(host, port, name, key)
        bot.run()
