import json
import hwomath


class HWODataHandler(object):
    def __init__(self, message_lines):
        self._message_lines = message_lines
        self._messages = [json.loads(x) for x in message_lines]
        self._game_init = None
        self._car_data = None
        self.game_id = None
        self.piece_lane_length_ranges = []


    @property
    def messages(self):
        return self._messages

    @property
    def game_init(self):
        if self._game_init is None:
            self._game_init = next(x for x in self._messages if x["msgType"] == "gameInit")
        return self._game_init

    @property
    def crashes(self):
        return [x for x in self.messages if x["msgType"] == "crash"]

    @property
    def pieces(self):
        return self.game_init["data"]["race"]["track"]["pieces"]

    @property
    def lanes(self):
        """Returns the lanes, sorted by lane index (in case they arrive unsorted)"""
        return sorted(self.game_init["data"]["race"]["track"]["lanes"],
                      key=lambda lane: lane["index"])

    @property
    def switches(self):
        """Returns an iterable with all the pieces that have switches"""
        return (x for x in self.pieces if x["switch"])

    @property
    def bends(self):
        """Returns an iterable with all the pieces that have bends"""
        return (x for x in self.pieces if x["angle"] != 0)

    @property
    def car_data(self):
        return self._car_data

    @property
    def car_positions(self):
        """Returns a list with the current car position records. Not using a
           generator in case new ones are added as we evaluate it, and so we
           can refer to items by their position.

           List is sorted by gameTick, just in case they were added out of order.
        """
        return sorted([x for x in self._messages if x["msgType"] == "carPositions" and "gameTick" in x],
                      key=lambda message: message['gameTick'])

    @staticmethod
    def get_angles(message_list):
        data = [x["data"] for x in message_list]
        return [x[0]["angle"] for x in data]

    @staticmethod
    def car_from_car_positions(car_name, piece_position_data):
        """
           Searches through the list of records received on the data for a carPositions
           message and returns the record belonging to the request car name
        """
        return next(x for x in piece_position_data if x["id"]["name"] == car_name)

    @staticmethod
    def piece_index_from_car_position(piece_position_data):
        """
           Extracts the index of the current piece from a piecePosition record,
           or returns -1 if it doesn't receive a valid one
        """
        if piece_position_data is None or 'piecePosition' not in piece_position_data:
            return -1
        else:
            return piece_position_data['piecePosition']['pieceIndex']

    def append_message_line(self, line):
        self._message_lines.append(line)
        parsed = json.loads(line)
        self._messages.append(parsed)
        return parsed

    def car_track_distance_at_tick(self, tick):
        """
            Returns the car's total distance in the track at a certain tick,
            calculated for the specific lane the car is on at that tick.
            If the tick isn't found, we just return 0.

            We could probably use this for acceleration and speed calculation
            as well, but would need to take into account that our car could
            do a lap between two ticks.
        """
        try:
            tick_message = next(x for x in self.car_positions if x["gameTick"] == tick)
        except StopIteration:
            return 0

        car_name = self.car_data["name"]
        in_piece_data = HWODataHandler.car_from_car_positions(car_name, tick_message["data"])
        piece_index = in_piece_data["piecePosition"]["pieceIndex"]
        lane = in_piece_data["piecePosition"]["lane"]["startLaneIndex"]
        piece_distance = in_piece_data["piecePosition"]["inPieceDistance"]

        return self.piece_lane_length_ranges[piece_index][lane]["start"] + piece_distance

    def speed_at_tick(self, tick, sample_size=10, alpha=None):
        """ Calculates a vehicle's speed at a certain tick, based on a list of previous
            samples.  It assumes that:
            - The car data has been cached
            - We have piece information
            Speed is returned in distance per tick.
        """
        positions = self.car_positions
        try:
            tick_message = next(x for x in positions if x["gameTick"] == tick)
        except StopIteration:
            return 0
        tick_index = positions.index(tick_message)

        # Get the elements to evaluate. We get one more than the sample size, since we'll
        # need it to calculate the total distance traversed for the same sample.
        first_index = max(tick_index - sample_size - 1, 0)
        previous_positions = positions[first_index:tick_index - 1]

        car_name = self.car_data["name"]

        speed = 0
        if len(previous_positions) > 1:
            distances = []
            for i in range(0, len(previous_positions) - 1):
                # Get the tick data
                previous_rec = HWODataHandler.car_from_car_positions(car_name, previous_positions[i]["data"])
                current_rec = HWODataHandler.car_from_car_positions(car_name, previous_positions[i + 1]["data"])
                # Get the piece data
                previous_piece = previous_rec["piecePosition"]["pieceIndex"]
                current_piece = current_rec["piecePosition"]["pieceIndex"]
                # Get the distance
                previous_distance = previous_rec["piecePosition"]["inPieceDistance"]
                current_distance = current_rec["piecePosition"]["inPieceDistance"]
                # Calculate the distance
                if current_piece == previous_piece:
                    distances.append(current_distance - previous_distance)
                else:
                    # Get the lanes. We'll go for the ending lane, since we don't
                    # have a good way to calculate the distance when switching lanes
                    previous_lane = previous_rec["piecePosition"]["lane"]["endLaneIndex"]
                    # Piece length
                    previous_length = self.pieces[previous_piece]["lane_lengths"][previous_lane]
                    distances.append(previous_length - previous_distance + current_distance)
            speed = hwomath.ema(distances, alpha)

        return speed

    def acceleration_at_tick(self, tick, sample_size=10, alpha=None):
        """ Calculates a vehicle's acceleration at a certain tick, using
            the speed calculation at a number of ticks prior to it.
            It assumes that:
            - The car data has been cached
            - We have piece information
            Acceleration is returned in speed units per tick.
            I've done tests with a sample size of 10 and 50. They yield
            almost identical results, and 50 is significantly slower.
        """
        if tick > 2:
            acceleration = (self.speed_at_tick(tick) - self.speed_at_tick(tick - sample_size)) / sample_size
        else:
            acceleration = self.speed_at_tick(tick, sample_size)

        return acceleration

    def cache_car_data(self):
        """Caches our car data and other related information.
           Should be called after the yourCar message has been received (likely on game start).
        """
        car_message = next(x for x in self.messages if x["msgType"] == "yourCar")
        self._car_data = car_message["data"]
        self.game_id = car_message["gameId"]

    def import_lines(self, filename):
        with open(filename) as f:
            lines = f.readlines()
            for line in lines:
                self.append_message_line(line)

    def homogenize_track_information(self):
        """Takes a list of track pieces and ensures that they all end up
           containing the same information
        """
        for piece in self.pieces:
            if "angle" in piece:
                # We don' get any length for the piece, so add it
                piece["length"] = hwomath.arc_length(piece["radius"], piece["angle"])
                # And we should also add lengths for the different lanes
                # If we're turning to the left, then the outside lane gets shorter
                sign = -hwomath.sign(piece["angle"])
                piece["lane_lengths"] = [hwomath.arc_length(piece["radius"] + sign * lane["distanceFromCenter"],
                                                            piece["angle"])
                                         for lane in self.lanes]
            elif "length" in piece:
                # If the piece is not an an angle, just add lane lengths of the same length for all
                piece["lane_lengths"] = [piece["length"]] * len(self.lanes)
            else:
                # Odd... but let' not leave it empty
                piece["length"] = 0

            if not "switch" in piece:
                piece["switch"] = False
            if not "angle" in piece:
                piece["angle"] = 0

            # Finally, cache the index for ease of reference without having
            # to continue checking the index
            piece["index"] = self.pieces.index(piece)

    def homogenize_position_information(self):
        """Adds a gameTick of -1 to any car position lacking one (which is
           likely to be only the first one received
        """
        for message in self.car_positions:
            if not "gameTick" in message:
                message["gameTick"] = -1

    def homogenize_data(self):
        self.homogenize_track_information()
        self.calculate_total_lane_length()
        self.calculate_lane_angle_forces()
        self.homogenize_position_information()

    def calculate_lane_angle_forces(self):
        for piece in self.pieces:
            if piece["angle"] != 0:
                forces = []
                for length in piece["lane_lengths"]:
                    forces.append(piece["angle"] / length)
                piece["angle_forces"] = forces
            else:
                piece["angle_forces"] = [0] * len(self.lanes)

    def calculate_total_lane_length(self):
        """Pre-calculates the total lane length for each specific lane. Adds the
           piece index to each record so that we don't have to waste time with
           checking the index and can filter by it.
        """
        self.piece_lane_length_ranges = []
        total_distance = [0] * len(self.lanes)
        for i in range(0, len(self.pieces)):
            total_lane_lengths = []
            for j in range(0, len(self.lanes)):
                piece_lane_data = {
                    "index": i,
                    "start": round(total_distance[j], 2),
                    "end": round(total_distance[j] + self.pieces[i]["lane_lengths"][j], 2)
                }
                total_lane_lengths.append(piece_lane_data)
                total_distance[j] = piece_lane_data["end"]
            self.piece_lane_length_ranges.append(total_lane_lengths)

    def get_messages_before(self, message, msg_type, count):
        return self.get_messages_before_tick(message["gameTick"], msg_type, count) if "gameTick" in message else []

    def get_messages_before_tick(self, tick, msg_type, count):
        return [x for x in self.messages
                if "gameTick" in x
                   and x["msgType"] == msg_type
            and tick - count <= x["gameTick"] < tick
        ]

    def angle_history_at_tick(self, tick, sample_count):
        leading = self.get_messages_before_tick(tick + 1, "carPositions", sample_count)
        return HWODataHandler.get_angles(leading)

    def angle_acceleration_at_tick(self, tick, sample_count):
        history = self.angle_history_at_tick(tick, sample_count)
        return (history[-1] - history[0]) / len(history) if history else 0

    def average_angles_at_tick(self, tick, sample_count, alpha=None):
        return hwomath.ema(self.angle_history_at_tick(tick, sample_count), alpha)


