import sys
import hwodata


class TrackReader(object):
    def __init__(self, filename):
        print "Reading from ", filename
        self.message_handler = hwodata.HWODataHandler([])
        self.message_handler.import_lines(filename)
        # Homogenize track piece information
        self.message_handler.homogenize_track_information()

    def print_crashes(self):
        for crash in self.message_handler.crashes():
            print crash

    def add_up_track_angles(self, look_ahead=2):
        """Adds up all the angles a number of pieces ahead,
           and returns them as an array on the same order as the pieces"""
        angles = []
        for i in range(len(self.message_handler.pieces)):
            angle = 0
            next_pieces = self.message_handler.pieces[i + 1:i + look_ahead + 1]
            for piece in next_pieces:
                if "angle" in piece:
                    angle = angle + piece["angle"]
            angles.append(angle)
        return angles

    def calculate_danger_ratings(self, angle_list, max_rating=85):
        danger_ratings = []
        # Second pass .. each rating gets averaged with the one for the previous piece
        for i in range(0, len(angle_list)):
            angle = (angle_list[i] + angle_list[i - 1]) / 2
            danger_ratings.append(abs(angle))
        # Third pass ... lower the danger rating if the next piece is a straight
        for i in range(0, len(danger_ratings) - 1):
            if self.message_handler.pieces[i + 1]["angle"] == 0:
                danger_ratings[i] /= 2
        # Finally, clamp
        for i in range(0, len(danger_ratings) - 1):
            danger_ratings[i] = min(danger_ratings[i], max_rating)
        return danger_ratings


if __name__ == "__main__":
    reader = TrackReader(sys.argv[1])
    reader.print_crashes()
    crashes = reader.message_handler.crashes()
    print "-- Evaluating ", crashes[0]
    leading = reader.message_handler.get_messages_before(crashes[0], "carPositions", 10)
    for msg in leading:
        print msg

    print "-- Angles --"
    for _crash in crashes:
        tick = _crash["gameTick"] - 1
        print "Average for %d: 3 -> %d / 5 -> %d / 10 -> %d" % \
              (tick,
               reader.message_handler.average_angles_at_tick(tick, 3),
               reader.message_handler.average_angles_at_tick(tick, 5),
               reader.message_handler.average_angles_at_tick(tick, 10))

    print "-- Track pieces --"
    angles = reader.add_up_track_angles()
    ratings = reader.calculate_danger_ratings(angles)
    i = 0
    for piece in reader.message_handler.pieces:
        print angles[i], ratings[i], piece
        i += 1
