import sys
import signal
import random
from LaneSwitcher import LaneSwitcher


class RandomWalk(LaneSwitcher):
    """
        Adds a random value to the last target throttle based on a smoothed
        scalar walk, just to get some data from the track.
    """
    def __init__(self, host, port, name, key, throttle, variance=0.25):
        super(RandomWalk, self).__init__(host, port, name, key, throttle)
        self.switch_on_piece = -1
        self.next_lane = None
        self.variance = float(0.25)
        self.random_smoothing = 0.05
        self.target_throttle = self.constant_throttle
        self.min_throttle = 0.1

    def calculate_throttle(self):
        random_throttle = random.random() * self.variance * 2 - self.variance
        self.target_throttle += random_throttle * self.random_smoothing
        self.target_throttle = max(self.target_throttle, self.min_throttle)
        return self.target_throttle

    def on_crash(self, data):
        super(RandomWalk, self).on_crash(data)
        # Reset the target throttle on crash to avoid it happening immediately again
        self.target_throttle = self.constant_throttle


def signal_handler(signal, frame):
        print "Saving logs before abort"
        bot.save_data_logs()
        print "Done"
        sys.exit(signal)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    if len(sys.argv) != 7:
        print("Usage: ./run host port botname botkey throttle variance")
    else:
        host, port, name, key, throttle, variance = sys.argv[1:7]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        for i in range(0, 5):
            bot = RandomWalk(host, port, name, key, throttle, variance)
            bot.run()
