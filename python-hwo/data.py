import json


class DataLogger(object):
    def __init__(self, folder, log_name, tag, log_id, log_type):
        """Initializes a log. Expects the log_type to be either csv or json.
           For CSV files it'll assume that the first item added contains the
           record definition, and will dump all records as tab-delimited
           items.
        """
        self.folder = folder
        self.log_name = log_name
        self.tag = tag
        self.log_id = log_id
        self.log_type = log_type
        self.filename = "{0}/{1}.{2}.{3}.{4}".format(folder, log_name, tag, log_id, log_type)
        self._items = []

    def add_item(self, item):
        self._items.append(item)

    def save_log(self):
        print "Saving data log to", self.filename
        if len(self._items) > 0:
            with open(self.filename, "w") as log_file:
                write_type_map = {
                    'csv': self.write_csv,
                    'json': self.write_json
                }
                if self.log_type in write_type_map:
                    write_type_map[self.log_type](log_file)
                else:
                    self.write_dump(log_file)
                log_file.close()

    def write_csv(self, log_file):
        # Take the keys off the first item
        first_record = self._items[0]
        log_file.write(",".join([key for key in first_record]))
        for item in self._items:
            log_file.write("\n")
            string_values = [str(item[key]) for key in item]
            log_file.write(",".join(string_values))

    def write_json(self, log_file):
        log_file.write(json.dumps(self._items))

    def write_dump(self, log_file):
        for item in self._items:
            log_file.write(str(item))
