open System
open System.IO
open System.Net.Sockets
open FSharp.Data

type Json = JsonProvider<"./sample.json", SampleIsList=true, RootName="message">

let send (msg : Json.Message) (writer : StreamWriter) =
    writer.WriteLine (msg.JsonValue.ToString(JsonSaveOptions.DisableFormatting))
    writer.Flush()

let join name key color =
    Json.Message(msgType = "join", data = Json.DecimalOrData(Json.Data(name = Some(name), key = Some(key), color = Some(color), race = None)))

let throttle (value : Decimal) =
    printfn "Throttling..."
    Json.Message(msgType = "throttle", data = Json.DecimalOrData(value))

let ping =
    Json.Message(msgType = "ping", data = null)

let strTitle (title: string, s: 'T option) : string =
    match s with
    | Some(s) -> String.Format("{0}: {1}", title, s).ToString()
    | None -> String.Format("No {0}", title).ToString()


let logInit (msg : Json.Message) =
    printfn "Received init"
    printfn "Type: %s" msg.MsgType
    printfn "Track: %s" msg.Data.Record.Value.Race.Value.Track.Name
    printfn "Total pieces: %d" msg.Data.Record.Value.Race.Value.Track.Pieces.Length

    for piece in msg.Data.Record.Value.Race.Value.Track.Pieces do
        let line =  
            [strTitle("Length", piece.Length); strTitle("Angle", piece.Angle); strTitle("Switch", piece.Switch)] 
            |> String.concat ", "
        printfn "-- %s" line

[<EntryPoint>]
let main args =
    let (host, portString, botName, botKey) = 
      args |> (function [|a;b;c;d|] -> a, b, c, d | _ -> failwith "Invalid param array")
    let port = Convert.ToInt32(portString)

    printfn "Connecting to %s:%d as %s/%s" host port botName botKey

    use client = new TcpClient(host, port)
    let stream = client.GetStream()
    let reader = new StreamReader(stream)
    let writer = new StreamWriter(stream)

    send (join botName botKey "blue") writer

    let processNextLine nextLine =
        match nextLine with
        | null -> false
        | line ->
            try 
                printfn "New Line: %s" line
                let msg = Json.Parse(line)
                match msg.MsgType with
                | "carPositions" -> send (throttle 0.65m) writer
                | "join" -> printfn "Joined"; send (ping) writer
                | "gameInit" -> logInit msg; send (ping) writer
                | "gameEnd" -> printfn "Race ends"; send (ping) writer
                | "gameStart" -> printfn "Race starts"; send (ping) writer
                | _ -> send (ping) writer
                true
            with
            | ex -> true

    while processNextLine (reader.ReadLine()) do 
        ()

    0